import { ApiProperty } from "@nestjs/swagger";
import { AuthenDto } from "src/controllers/authen/dto/authen.dto";

export class AuthenServiceDto {
  @ApiProperty({ type: String })
  username: string
  @ApiProperty({ type: String })
  password: string
  static fromAuthenServiceDto(authenDto: AuthenDto): AuthenServiceDto {
    const authenServiceDto = new AuthenServiceDto()
    authenServiceDto.username = authenDto.username
    authenServiceDto.password = authenDto.password
    return authenServiceDto
  }
}


