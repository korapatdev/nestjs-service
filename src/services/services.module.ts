import { Module } from '@nestjs/common';
import { UserEntityModule } from 'src/resoures/user-entity/user-entity.module';
import { ServicesService } from './services.service';

@Module({
  imports: [
    UserEntityModule
  ],
  providers: [ServicesService],
  exports: [ServicesService]
})
export class ServicesModule { }
