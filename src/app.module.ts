import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthenModule } from './controllers/authen/authen.module';
import { CBackOfficeModule } from './controllers/c-back-office/c-back-office.module';
import { CFrontEndModule } from './controllers/c-front-end/c-front-end.module';
import { CUnityModule } from './controllers/c-unity/c-unity.module';
import { ServicesModule } from './services/services.module';
import { WebsocketModule } from './controllers/websocket/websocket.module';
import { AppService } from './app.service';


@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true
      // envFilePath: `.env.${process.env.NODE_ENV}`,
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,

      autoLoadEntities: true,
      // migrationsRun:true
      synchronize: true,
      logging: true,
      
    }),
    AuthenModule, 
    CBackOfficeModule,
    CFrontEndModule,
    CUnityModule,
    WebsocketModule,
  ],
  providers: [AppService],
})
export class AppModule { }
