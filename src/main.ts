import { NestFactory } from '@nestjs/core';
import { DocumentBuilder } from '@nestjs/swagger';
import { SwaggerModule } from '@nestjs/swagger/dist';
import { AppModule } from './app.module';
import { AuthenModule } from './controllers/authen/authen.module';
import { CBackOfficeModule } from './controllers/c-back-office/c-back-office.module';
import { CFrontEndModule } from './controllers/c-front-end/c-front-end.module';
import { CUnityModule } from './controllers/c-unity/c-unity.module';
import { WebsocketModule } from './controllers/websocket/websocket.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const config = new DocumentBuilder()
    .setTitle('Hello my world : Model MVP ')
    .setDescription('My World API Description')
    .setVersion('1.0')
    .addBearerAuth({
      type: 'http',
      scheme: 'bearer',
      bearerFormat: 'JWT',
      description: 'Enter your access token to authenticate your requests.',
    })
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api-myworld', app, document);
  await app.listen(3030);
}
bootstrap();
