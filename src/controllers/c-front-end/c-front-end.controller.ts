import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger/dist/decorators';
import { CFrontEndService } from './c-front-end.service';
import { CreateCFrontEndDto } from './dto/create-c-front-end.dto';

@ApiTags('Front end')
@Controller('c-front-end')
export class CFrontEndController {
  constructor(private readonly cFrontEndService: CFrontEndService) {}

  @Post()
  create(@Body() createCFrontEndDto: CreateCFrontEndDto) {
    return this.cFrontEndService.create(createCFrontEndDto);
  }

  @Get()
  findAll() {
    return this.cFrontEndService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.cFrontEndService.findOne(+id);
  }



  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.cFrontEndService.remove(+id);
  }
}
