import { Module } from '@nestjs/common';
import { CFrontEndService } from './c-front-end.service';
import { CFrontEndController } from './c-front-end.controller';
import { ServicesModule } from 'src/services/services.module';

@Module({
  imports: [
    ServicesModule
  ],
  controllers: [CFrontEndController],
  providers: [CFrontEndService]
})
export class CFrontEndModule {}
