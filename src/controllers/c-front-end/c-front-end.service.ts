import { Injectable } from '@nestjs/common';
import { CreateCFrontEndDto } from './dto/create-c-front-end.dto';


@Injectable()
export class CFrontEndService {
  create(createCFrontEndDto: CreateCFrontEndDto) {
    return 'This action adds a new cFrontEnd';
  }

  findAll() {
    return `This action returns all cFrontEnd`;
  }

  findOne(id: number) {
    return `This action returns a #${id} cFrontEnd`;
  }


  remove(id: number) {
    return `This action removes a #${id} cFrontEnd`;
  }
}
