import { Module } from '@nestjs/common';
import { CBackOfficeService } from './c-back-office.service';
import { CBackOfficeController } from './c-back-office.controller';
import { ServicesModule } from 'src/services/services.module';

@Module({
  imports: [
    ServicesModule
  ],
  controllers: [CBackOfficeController],
  providers: [CBackOfficeService]
})
export class CBackOfficeModule {}
