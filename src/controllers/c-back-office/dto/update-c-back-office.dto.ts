import { PartialType } from '@nestjs/mapped-types';
import { CreateCBackOfficeDto } from './create-c-back-office.dto';

export class UpdateCBackOfficeDto extends PartialType(CreateCBackOfficeDto) {}
