import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger/dist';
import { CBackOfficeService } from './c-back-office.service';
import { CreateCBackOfficeDto } from './dto/create-c-back-office.dto';
import { UpdateCBackOfficeDto } from './dto/update-c-back-office.dto';

@ApiTags('Back office')
@Controller('c-back-office')
export class CBackOfficeController {
  constructor(private readonly cBackOfficeService: CBackOfficeService) {}

  @Post()
  create(@Body() createCBackOfficeDto: CreateCBackOfficeDto) {
    return this.cBackOfficeService.create(createCBackOfficeDto);
  }

  @Get()
  findAll() {
    return this.cBackOfficeService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.cBackOfficeService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCBackOfficeDto: UpdateCBackOfficeDto) {
    return this.cBackOfficeService.update(+id, updateCBackOfficeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.cBackOfficeService.remove(+id);
  }
}
