import { Injectable } from '@nestjs/common';
import { CreateCBackOfficeDto } from './dto/create-c-back-office.dto';
import { UpdateCBackOfficeDto } from './dto/update-c-back-office.dto';

@Injectable()
export class CBackOfficeService {
  create(createCBackOfficeDto: CreateCBackOfficeDto) {
    return 'This action adds a new cBackOffice';
  }

  findAll() {
    return `This action returns all cBackOffice`;
  }

  findOne(id: number) {
    return `This action returns a #${id} cBackOffice`;
  }

  update(id: number, updateCBackOfficeDto: UpdateCBackOfficeDto) {
    return `This action updates a #${id} cBackOffice`;
  }

  remove(id: number) {
    return `This action removes a #${id} cBackOffice`;
  }
}
