import { Injectable } from '@nestjs/common';
import { CreateCUnityDto } from './dto/create-c-unity.dto';
import { UpdateCUnityDto } from './dto/update-c-unity.dto';

@Injectable()
export class CUnityService {
  create(createCUnityDto: CreateCUnityDto) {
    return 'This action adds a new cUnity';
  }

  findAll() {
    return `This action returns all cUnity`;
  }

  findOne(id: number) {
    return `This action returns a #${id} cUnity`;
  }

  update(id: number, updateCUnityDto: UpdateCUnityDto) {
    return `This action updates a #${id} cUnity`;
  }

  remove(id: number) {
    return `This action removes a #${id} cUnity`;
  }
}
