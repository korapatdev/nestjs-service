import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger/dist/decorators';
import { CUnityService } from './c-unity.service';
import { CreateCUnityDto } from './dto/create-c-unity.dto';
import { UpdateCUnityDto } from './dto/update-c-unity.dto';

@ApiTags('Untity')
@Controller('c-unity')
export class CUnityController {
  constructor(private readonly cUnityService: CUnityService) {}

  @Post()
  create(@Body() createCUnityDto: CreateCUnityDto) {
    return this.cUnityService.create(createCUnityDto);
  }

  @Get()
  findAll() {
    return this.cUnityService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.cUnityService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCUnityDto: UpdateCUnityDto) {
    return this.cUnityService.update(+id, updateCUnityDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.cUnityService.remove(+id);
  }
}
