import { Module } from '@nestjs/common';
import { CUnityService } from './c-unity.service';
import { CUnityController } from './c-unity.controller';
import { ServicesModule } from 'src/services/services.module';

@Module({
  imports: [
    ServicesModule
  ],
  controllers: [CUnityController],
  providers: [CUnityService]
})
export class CUnityModule { }
