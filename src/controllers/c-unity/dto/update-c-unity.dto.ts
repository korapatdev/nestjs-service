import { PartialType } from '@nestjs/mapped-types';
import { CreateCUnityDto } from './create-c-unity.dto';

export class UpdateCUnityDto extends PartialType(CreateCUnityDto) {}
