import { Module } from '@nestjs/common';
import { ServicesModule } from 'src/services/services.module';
import { WebsocketGateway } from './websocket.gateway';

@Module({
  imports: [
    ServicesModule
  ],
  providers: [WebsocketGateway]
})
export class WebsocketModule { }
