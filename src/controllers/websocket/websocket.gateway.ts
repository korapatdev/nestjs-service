import { WebSocketGateway, SubscribeMessage, MessageBody, OnGatewayConnection, OnGatewayDisconnect, WebSocketServer } from '@nestjs/websockets';
import { CreateWebsocketDto } from './dto/create-websocket.dto';
import { Server } from 'http';
import { Logger } from '@nestjs/common';
import { ServicesService } from 'src/services/services.service';


@WebSocketGateway()
export class WebsocketGateway implements OnGatewayConnection {

  @WebSocketServer() private server: any;
  private logger: Logger = new Logger('LeaderboardGateway');
  afterInit(server: Server) {
    this.logger.log("Server Init");
  }
  handleConnection(client: any) {
    console.log('Client connected');
  }

  constructor(private readonly servicesService: ServicesService) { }

  // @SubscribeMessage('createWebsocket')
  // create(@MessageBody() createWebsocketDto: CreateWebsocketDto) {
  //   return this.websocketService.create(createWebsocketDto);
  // }

  // @SubscribeMessage('findAllWebsocket')
  // findAll() {
  //   return this.websocketService.findAll();
  // }

  // @SubscribeMessage('findOneWebsocket')
  // findOne(@MessageBody() id: number) {
  //   return this.websocketService.findOne(id);
  // }

  // // @SubscribeMessage('updateWebsocket')
  // // update(@MessageBody() updateWebsocketDto: UpdateWebsocketDto) {
  // //   return this.websocketService.update(updateWebsocketDto.id, updateWebsocketDto);
  // // }

  // @SubscribeMessage('removeWebsocket')
  // remove(@MessageBody() id: number) {
  //   return this.websocketService.remove(id);
  // }
}
