import { Injectable } from '@nestjs/common';
import { AuthenServiceDto } from 'src/services/dto/authen-services.dto';
import { ServicesService } from 'src/services/services.service';
import { AuthenDto } from './dto/authen.dto';



@Injectable()
export class AuthenService {
  constructor(private readonly servicesService: ServicesService) {}

  async register(authenDto: AuthenDto){
    const authenServiceDto = AuthenServiceDto.fromAuthenServiceDto(authenDto)
    return await this.servicesService.registerService(authenServiceDto)
  }
}
