class ResponsePresenter<T> {
  public status: number;
  public message: string;
  public result: T | T[]

  constructor({status, message, result}:ResponsePresenter<T>) {
    this.status = status;
    this.message = message;
    this.result = result;
    return this;
  }
}
export default ResponsePresenter;