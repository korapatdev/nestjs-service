import { ApiProperty } from "@nestjs/swagger";

export class AuthenDto {
  @ApiProperty({ type: String })
  username: string
  @ApiProperty({ type: String })
  password: string
}


