import { Controller, Get, Post, Body, Patch, Param, Delete, HttpException, UseFilters } from '@nestjs/common';
import { HttpCode } from '@nestjs/common/decorators';
import { ApiTags } from '@nestjs/swagger';
import { HttpExceptionFilter } from '../../middlewares/http-exception.filter';
import { JoiValidationPipe } from '../../middlewares/validation-pipe';
import { AuthenService } from './authen.service';
import { AuthenDto } from './dto/authen.dto';
import ResponsePresenter from './presenters/respones.presenter';
import { AuthenValidator } from './validators/register.validators';

@ApiTags('Authentication')
@Controller('authentication')
export class AuthenController {
  constructor(private readonly authenService: AuthenService) {}

  @Post('register')
  @UseFilters(new HttpExceptionFilter())
  @HttpCode(201)
  async register(
    @Body(new JoiValidationPipe(AuthenValidator)) authenDto: AuthenDto
  ): Promise<ResponsePresenter<any>> {
    try {
      const result = await this.authenService.register(authenDto)
      return new ResponsePresenter({
        status: 200,
        message: 'Sign up successfully',
        result: 'test'
      })
    } catch (error) {
      throw new HttpException(
        {
          status: error.status,
          error: error.error,
          message: error.message,
        },
        error.status,
      );
    }
  }

  @Post('login')
  @UseFilters(new HttpExceptionFilter())
  @HttpCode(200)
  async login(
    @Body(new JoiValidationPipe(AuthenValidator)) authenDto: AuthenDto
  ): Promise<ResponsePresenter<any>> {
    try {
      return new ResponsePresenter({
        status: 200,
        message: 'login success',
        result: 'test'
      })
    } catch (error) {
      throw new HttpException(
        {
          status: error.status,
          error: error.error,
          message: error.message,
        },
        error.status,
      );
    }
  }
}
