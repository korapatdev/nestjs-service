import * as Joi from 'joi';

export const AuthenValidator = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().min(8).required(),
});
