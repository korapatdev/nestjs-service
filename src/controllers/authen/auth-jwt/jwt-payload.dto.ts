import { Role } from "./role";

export class JwtPayloadDto {
  public id: string;
  public username: string;
  public role: Role;
  public token: string;
  public iat?: number;

  static fromJSON(json: any): JwtPayloadDto {
    return Object.assign(JSON.parse(JSON.stringify(json)));
  }
}