import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { JwtPayloadDto } from './jwt-payload.dto';
import { ServicesService } from 'src/services/services.service';
// console.log("strategy log");

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private servicesService: ServicesService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET_KEY,
    });
  }

  async validate(token: string , payload:JwtPayloadDto): Promise<JwtPayloadDto> {
    // const authHeader = request.headers.get('Authorization');

    const checkValidate = await this.servicesService.ValidateUser(token);
    return {
      id: payload.id,
      username: payload.username,
      role: payload.role,
      token: payload.token,
      iat: payload.iat
    };
  }
}