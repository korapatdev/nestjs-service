import { Module } from '@nestjs/common';
import { AuthenService } from './authen.service';
import { AuthenController } from './authen.controller';
import { ServicesModule } from '../../services/services.module'
import { PassportModule } from '@nestjs/passport/dist';

@Module({
  imports: [
    ServicesModule ,
    PassportModule
  ],
  controllers: [AuthenController],
  providers: [AuthenService]
})
export class AuthenModule { }
