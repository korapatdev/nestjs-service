import { Test, TestingModule } from '@nestjs/testing';
import { AuthenController } from './authen.controller';
import { AuthenService } from './authen.service';


describe('UsersController', () => {
  let authenController: AuthenController;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthenController],
      providers: [AuthenService],
    }).compile();

    authenController = module.get<AuthenController>(AuthenController);
  });
  it('should be defined', () => {
    expect(authenController).toBeDefined();
  });
});
