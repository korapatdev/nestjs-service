import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserEntityDto } from './dto/create-user-entity.dto';
import { UpdateUserEntityDto } from './dto/update-user-entity.dto';
import { UserEntity } from './entities/user.entity';

@Injectable()
export class UserEntityService {
  constructor(
    @InjectRepository(UserEntity) private userEntityRepository: Repository<UserEntity>,
  ) { }
  create(createUserEntityDto: CreateUserEntityDto) {
    return 'This action adds a new userEntity';
  }

  findAll() {
    return `This action returns all userEntity`;
  }

  findOne(id: number) {
    return `This action returns a #${id} userEntity`;
  }

  update(id: number, updateUserEntityDto: UpdateUserEntityDto) {
    return `This action updates a #${id} userEntity`;
  }

  remove(id: number) {
    return `This action removes a #${id} userEntity`;
  }
}
