import { PartialType } from '@nestjs/swagger';
import { CreateUserEntityDto } from './create-user-entity.dto';

export class UpdateUserEntityDto extends PartialType(CreateUserEntityDto) {}
